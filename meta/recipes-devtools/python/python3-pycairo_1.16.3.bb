SUMMARY = "Python bindings for the Cairo canvas library"
HOMEPAGE = "http://cairographics.org/pycairo"
BUGTRACKER = "http://bugs.freedesktop.org"
SECTION = "python-devel"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=f2e071ab72978431b294a0d696327421 \
                    file://COPYING-LGPL-2.1;md5=fad9b3332be894bab9bc501572864b29 \
                    file://COPYING-MPL-1.1;md5=bfe1f75d606912a4111c90743d6c7325 "

S = "${WORKDIR}/pycairo-${PV}"

# cairo >= 1.8.8
DEPENDS = "cairo"
PR = "r2"

SRC_URI = "https://github.com/pygobject/pycairo/releases/download/v${PV}/pycairo-${PV}.tar.gz"

SRC_URI[md5sum] = "d2a115037ccd128219f43d5ed3df7926"
SRC_URI[sha256sum] = "5bb321e5d4f8b3a51f56fc6a35c143f1b72ce0d748b43d8b623596e8215f01f7"

FILES_${PN}_append = " /usr/include /usr/lib/pkgconfig"

inherit setuptools3 pkgconfig

do_install_append() {
	install -d ${D}/usr/lib
	mv ${D}/usr/share/include ${D}/usr/
	mv ${D}/usr/share/lib/pkgconfig ${D}/usr/lib/
}

BBCLASSEXTEND = "native"
